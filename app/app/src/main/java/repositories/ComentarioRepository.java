package repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import entities.Artista;
import entities.Comentario;
import entities.Usuario;

@Repository
public interface ComentarioRepository extends CrudRepository<Comentario, Long> {
	
	public Comentario findById(int id);
	
	public List<Comentario> findByArtista(Artista artista);
	
	public List<Comentario> findByUsuario (Usuario usuario);
	
	public List<Comentario> findByTexto (String texto);
	
}