package repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import entities.Artista;

@Repository
public interface ArtistaRepository extends CrudRepository<Artista, Long> {
	
	public Artista findById(int id);
	
	public Artista findByNombre(String nombre);
	
	public List<Artista> findByDescripcion (String descripcion);
	
	public List<Artista> findByCategoria (String categoria);
	
	public List<Artista> findByImagen (String imagen);
		
	
}