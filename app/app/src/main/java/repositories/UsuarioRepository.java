package repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import entities.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	public Usuario findById(int id);
	
	public Usuario findByEmail(String email);
	
	public List<Usuario> findByNombre(String nombre);
	
	public List<Usuario> findByApellido(String apellido);
	
	public Usuario findByUsername(String username);

	
	
}