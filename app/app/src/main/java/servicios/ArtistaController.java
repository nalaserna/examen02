package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Artista;
import repositories.ArtistaRepository;

@RestController
public class ArtistaController {
	
	@Autowired
	private ArtistaRepository artistaRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping(path="/getAllArtistas", method=RequestMethod.GET)
	public Iterable<Artista> getAllArtistas () {
		
		Iterable<Artista> findAll = artistaRepositoryDAO.findAll();
		return findAll;
	}
	
	@CrossOrigin
	@RequestMapping(path="/addArtista", method=RequestMethod.POST) 
	public @ResponseBody String addArtista
	(@RequestParam String nombre, @RequestParam String descripcion, 
	@RequestParam String categoria, @RequestParam String imagen) 
			throws ParseException {
		
		Artista nuevoArtista = new Artista();
		nuevoArtista.setNombre(nombre);
		nuevoArtista.setDescripcion(descripcion);
		nuevoArtista.setCategoria(categoria);
		nuevoArtista.setImagen(imagen);
		
		artistaRepositoryDAO.save(nuevoArtista);
		return "Artista Guardado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteArtista", method=RequestMethod.POST) 
	public @ResponseBody String deleteArtista
	(@RequestParam String id) {
		
		int idartista = Integer.parseInt(id);
		
		Artista artista = artistaRepositoryDAO.findById(idartista);
		artistaRepositoryDAO.delete(artista);
		return "Artista Eliminado";
		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getArtistaById", method=RequestMethod.GET)
	public Artista getArtistaById (@RequestParam int id) {
		//int idusuario = Integer.parseInt(id);
		//System.out.println(idusuario);
		
		return artistaRepositoryDAO.findById(id);
	}
	
	@CrossOrigin
	@RequestMapping (path="/getArtistaByNombre", method=RequestMethod.GET)
	public Artista getArtistaByNombre (@RequestParam String nombre) {

		return artistaRepositoryDAO.findByNombre(nombre);
	
	}
	
	@CrossOrigin
	@RequestMapping (path="/getArtistaByCategoria", method=RequestMethod.GET)
	public Iterable<Artista> getArtistaByCategoria (@RequestParam String categoria) {

		Iterable<Artista> findCategoria = artistaRepositoryDAO.findByCategoria(categoria);
		return findCategoria;
	
	}
	
}
