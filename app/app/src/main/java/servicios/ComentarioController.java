package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Comentario;
import entities.Artista;
import entities.Usuario;

import repositories.ArtistaRepository;
import repositories.ComentarioRepository;
import repositories.UsuarioRepository;


@RestController
public class ComentarioController {
	
	@Autowired
	private ComentarioRepository comentarioRepositoryDAO;
	
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	
	@Autowired
	private ArtistaRepository artistaRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping(path="/getAllComentarios", method=RequestMethod.GET)
	public Iterable<Comentario> getAllComentarios () {
		
		Iterable<Comentario> findAll = comentarioRepositoryDAO.findAll();
		return findAll;
	}
	
	@CrossOrigin
	@RequestMapping(path="/addComentario", method=RequestMethod.POST) 
	public @ResponseBody String addComentario
	(@RequestParam String artista_id, @RequestParam String username, 
	@RequestParam String texto) 
			throws ParseException {
		
		int idartista = Integer.parseInt(artista_id);
		Artista artista = artistaRepositoryDAO.findById(idartista);
		
		Usuario usuario = usuarioRepositoryDAO.findByUsername(username);
		
		Comentario nuevoComentario = new Comentario();
		nuevoComentario.setArtista(artista);
		nuevoComentario.setUsuario(usuario);
		nuevoComentario.setTexto(texto);
		
		comentarioRepositoryDAO.save(nuevoComentario);
		return "Comentario Guardado";
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteComentario", method=RequestMethod.POST) 
	public @ResponseBody String deleteComentario
	(@RequestParam String id) {
		
		int idcomment = Integer.parseInt(id);
		
		Comentario comentario = comentarioRepositoryDAO.findById(idcomment);
		comentarioRepositoryDAO.delete(comentario);
		return "Comentario Eliminado";
		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getCommentById", method=RequestMethod.GET)
	public Comentario getCommentById (@RequestParam int id) {
		//int idusuario = Integer.parseInt(id);
		//System.out.println(idusuario);
		
		return comentarioRepositoryDAO.findById(id);
	}
	
	@CrossOrigin
	@RequestMapping (path="/getCommentsByArtista", method=RequestMethod.GET)
	public Iterable<Comentario> getCommentsByArtista (@RequestParam String id_artista) {

		int idartista = Integer.parseInt(id_artista);
		Artista artista = artistaRepositoryDAO.findById(idartista);
		Iterable<Comentario> comentarios = comentarioRepositoryDAO.findByArtista(artista);
		
		return comentarios;
	
	}
	
	@CrossOrigin
	@RequestMapping (path="/getCommentsByUsuario", method=RequestMethod.GET)
	public Iterable<Comentario> getCommentsByUsuario (@RequestParam String id_usuario) {
	
		int idusuario = Integer.parseInt(id_usuario);
		Usuario usuario = usuarioRepositoryDAO.findById(idusuario);
		Iterable<Comentario> comentarios = comentarioRepositoryDAO.findByUsuario(usuario);
		
		return comentarios;
	}
	
}
