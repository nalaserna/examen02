package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Usuario;
import repositories.UsuarioRepository;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;

	@RequestMapping(path = "/getAllUsers", method = RequestMethod.GET)
	public Iterable<Usuario> getAllUsers() {

		Iterable<Usuario> findAll = usuarioRepositoryDAO.findAll();
		return findAll;

	}

	@CrossOrigin
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public @ResponseBody Usuario login(@RequestParam String username, @RequestParam String password)
			throws ParseException {

		Usuario usuario = usuarioRepositoryDAO.findByUsername(username);
		Usuario retorno = new Usuario();
		retorno.setNombre("error");
		retorno.setApellido("error");
		retorno.setEmail("error");
		retorno.setUsername("error");
		retorno.setPwd("error");
		if (usuario != null) {
			if (usuario.getPwd().equals(password) == true) {
				retorno = usuario;
			} 
		} 
		System.out.println(usuario.getUsername());
			return retorno;
	}

	@CrossOrigin
	@RequestMapping(path = "/addUser", method = RequestMethod.POST)
	public @ResponseBody String addNewUser(@RequestParam String nombre, @RequestParam String apellido,
			@RequestParam String email, @RequestParam String username, @RequestParam String pwd) throws ParseException {

		Usuario nuevoUsuario = new Usuario();
		nuevoUsuario.setNombre(nombre);
		nuevoUsuario.setApellido(apellido);
		nuevoUsuario.setEmail(email);
		nuevoUsuario.setPwd(pwd);
		nuevoUsuario.setUsername(username);

		usuarioRepositoryDAO.save(nuevoUsuario);
		return "Usuario Guardado";

	}

	@CrossOrigin
	@RequestMapping(path = "/deleteUser", method = RequestMethod.POST)
	public @ResponseBody String deleteUser(@RequestParam String id) {

		int idusuario = Integer.parseInt(id);

		Usuario usuario = usuarioRepositoryDAO.findById(idusuario);
		usuarioRepositoryDAO.delete(usuario);
		return "Usuario Eliminado";

	}

	@CrossOrigin
	@RequestMapping(path = "/getUserById", method = RequestMethod.GET)
	public Usuario getUserById(@RequestParam int id) {
		// int idusuario = Integer.parseInt(id);
		// System.out.println(idusuario);

		return usuarioRepositoryDAO.findById(id);
	}

	@CrossOrigin
	@RequestMapping(path = "/getUserByEmail", method = RequestMethod.GET)
	public Usuario getUserByEmail(@RequestParam String email) {

		return usuarioRepositoryDAO.findByEmail(email);

	}

	@CrossOrigin
	@RequestMapping(path = "/getUserByUsername", method = RequestMethod.GET)
	public Usuario getUserByUsername(@RequestParam String username) {

		return usuarioRepositoryDAO.findByUsername(username);

	}

}
