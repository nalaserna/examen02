import { Injectable } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { Usuario } from 'src/app/model/Usuario';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Comentario } from 'src/app/model/Comentario';


@Injectable({
  providedIn: 'root'
})
export class TechnoService {
  [x: string]: any;

  constructor(private http: HttpClient) { }

  public getAllArtistas(): Observable<Artista[]> {
    return this.http.get<Artista[]>(environment.urlConsultarArtistas);
  }

  public registerUser(usuario: Usuario) {
    const body = new HttpParams().set('nombre', usuario.nombre + '')
      .set('apellido', usuario.apellido + '')
      .set('pwd', usuario.pwd + '')
      .set('username', usuario.username + '')
      .set('email', usuario.email);
    return this.http.post(environment.urlRegister, body).subscribe();
}



  public viewArtista(artistaid: number): Observable<Artista>{
    const options = { params: new HttpParams().set('id', artistaid+'')};
    return this.http.get<Artista>(environment.urlViewArtista, options);
    console.log("Sirvio");
  }

  public newArtista(artista: Artista){
    console.log("Artista a insertar: ", artista);
    const body = new HttpParams().set('nombre', artista.nombre+'').set('descripcion', artista.descripcion).set('categoria', artista.categoria+'').set('imagen', artista.imagen+'');

    return this.http.post(environment.urlNuevoArtista, body).subscribe();
  }

  public getCommentsByArtist(artistaid: number): Observable<Comentario[]>{
    const options = { params: new HttpParams().set('id_artista', artistaid+'')};
    return this.http.get<Comentario[]>(environment.getCommentsByArtist, options);
    console.log("Sirvio");
  }

  public insertComment(comment: Comentario){
    console.log("Comentario a insertar: ", comment);
    const body = new HttpParams().set('username', comment.usuario.username+'').set('texto', comment.texto).set('artista_id', comment.artista.id+'');

    return this.http.post(environment.urlNuevoComment, body).subscribe();
  }

  public getUsuarioById(id: number): Observable<Usuario>{
    console.log("El id_usuario es: ", id);
    const options = { params: new HttpParams().set('id', id+'') };

    return this.http.get<Usuario>(environment.urlConsultarUsuario, options);
  }

  public autenticar(username: string, password:string){
    const body = new HttpParams().set('username', username+'').set('password', password+'');
    return this.http.post<Usuario>(environment.urlLogin, body);
  }

  public getUsuarioByUsername(username: string): Observable<Usuario>{
    console.log("El username es: ", username);
    const options = { params: new HttpParams().set('username', username+'') };

    return this.http.get<Usuario>(environment.urlGetUsuarioByUsername, options);
  }


  }

