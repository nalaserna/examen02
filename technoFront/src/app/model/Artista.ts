export class Artista{
    id: number;
    nombre: string;
    descripcion: string;
    categoria: string;
    imagen: string;
}