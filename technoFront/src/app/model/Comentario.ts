import { Usuario } from './Usuario';
import { Artista } from './Artista';

export class Comentario{
    id:number;
    texto: string;
    usuario: Usuario;
    artista: Artista;
}