import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCommentsComponent } from './detalle-comments.component';

describe('DetalleCommentsComponent', () => {
  let component: DetalleCommentsComponent;
  let fixture: ComponentFixture<DetalleCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
