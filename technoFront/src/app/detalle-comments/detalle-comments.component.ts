import { Component, OnInit, Input } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { Usuario } from 'src/app/model/Usuario';
import { Comentario } from 'src/app/model/Comentario';

@Component({
  selector: 'app-detalle-comments',
  templateUrl: './detalle-comments.component.html',
  styleUrls: ['./detalle-comments.component.css']
})
export class DetalleCommentsComponent implements OnInit {

  constructor() { }
  @Input() mySelectedComment: Comentario;
  @Input() mySelectedArtista : Artista;
  @Input() user: Usuario;
  ngOnInit() {
  
  }

}
