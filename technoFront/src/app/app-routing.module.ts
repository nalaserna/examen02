import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArtistaProfileComponent } from './artista-profile/artista-profile.component';
import { DetalleArtistaComponent } from './detalle-artista/detalle-artista.component';
import { DetalleCommentsComponent } from './detalle-comments/detalle-comments.component';
import { ListaCommentsComponent } from './lista-comments/lista-comments.component';
import { ListaArtistasComponent } from './lista-artistas/lista-artistas.component';
import { CreateArtistaComponent } from './create-artista/create-artista.component';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import { LoginComponent } from 'src/app/login/login.component';
import { RegistroComponent } from 'src/app/registro/registro.component';
import { LandingpageComponent } from './landingpage/landingpage.component';

const routes: Routes = [
  {path:'artista-profile/:id', component:ArtistaProfileComponent},
  {path: 'home', component: ListaArtistasComponent },
  {path:'new-artista', component: CreateArtistaComponent},
  {path:'new-comment', component: CreateCommentComponent},
  {path:'register', component: RegistroComponent},
  {path:'login', component: LoginComponent},
  {path:'welcome', component: LandingpageComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'welcome'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
