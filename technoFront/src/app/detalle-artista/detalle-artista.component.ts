import { Component, OnInit, Input } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { Usuario } from 'src/app/model/Usuario';

@Component({
  selector: 'app-detalle-artista',
  templateUrl: './detalle-artista.component.html',
  styleUrls: ['./detalle-artista.component.css']
})
export class DetalleArtistaComponent implements OnInit {

  constructor() { }
  @Input() mySelectedArtista: Artista;
  @Input() user: Usuario;
 
  @Input() detalle: boolean;
  ngOnInit() {
  }


}
