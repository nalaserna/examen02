import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/services/session.service';
import { Router, NavigationEnd } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { TechnoService } from 'src/services/techno.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user_name: string;
  pass: string;
  retorno: Usuario;

  constructor(private sessionService: SessionService, private technoService: TechnoService, private router: Router) {
  }

  ngOnInit() {
    let activo = window.localStorage.getItem('username')
    console.log(activo);

    if (activo != null)
      this.router.navigateByUrl('/home');
    else this.router.navigateByUrl('/login');
  }



  logIn() {
    this.technoService.autenticar(this.user_name, this.pass).subscribe(text => {
      console.log(text);
      this.retorno = text;
      if (this.retorno.username != "error"){
        window.localStorage.setItem('username', this.retorno.username);
        this.router.navigateByUrl('/home');
      }else{
        alert("Usuario/Contraseña incorrecta")

      }
    });

    
  }

}