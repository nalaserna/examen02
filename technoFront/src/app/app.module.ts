import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaArtistasComponent } from './lista-artistas/lista-artistas.component';
import { ListaCommentsComponent } from './lista-comments/lista-comments.component';
import { DetalleArtistaComponent } from './detalle-artista/detalle-artista.component';
import { DetalleCommentsComponent } from './detalle-comments/detalle-comments.component';
import { ArtistaProfileComponent } from './artista-profile/artista-profile.component';
import { CreateArtistaComponent } from './create-artista/create-artista.component';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import {HttpClientModule, HttpClient } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider'; 
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { SessionService } from 'src/services/session.service';
import { LandingpageComponent } from './landingpage/landingpage.component';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [
    AppComponent,
    ListaArtistasComponent,
    ListaCommentsComponent,
    DetalleArtistaComponent,
    DetalleCommentsComponent,
    ArtistaProfileComponent,
    CreateArtistaComponent,
    CreateCommentComponent,
    LoginComponent,
    RegistroComponent,
    LandingpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatDividerModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    FormsModule,
    MatGridListModule

  ],
  providers: [
    SessionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
