import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/Usuario';
import { TechnoService } from 'src/services/techno.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usr: Usuario;
  nombre: string;
  apellido: string;
  email: string;
  username: string;
  pwd: string;

  constructor(private technoService: TechnoService, private router: Router ) {
  
  }

  ngOnInit() {
  }

  registerUser() {
    this.usr = {
      nombre: this.nombre,
      apellido: this.apellido,
      email: this.email,
      username: this. username,
      pwd: this.pwd
    };

    console.log('Agregar Usuario ' + this.usr.nombre);
    this.technoService.registerUser(this.usr);
      this.router.navigateByUrl('/login')
      console.log("Sirvio");
  }
}