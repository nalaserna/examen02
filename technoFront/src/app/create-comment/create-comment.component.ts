import { Component, OnInit, Input } from '@angular/core';
import { TechnoService } from 'src/services/techno.service';
import { Usuario } from '../model/Usuario';
import { Artista } from '../model/Artista';
import { Comentario } from '../model/Comentario';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {
  miComment: Comentario;
  misComments: Array<Comentario>;
  user : Usuario;
  @Input() mySelectedArtista= Artista;
  
  constructor(private technoService: TechnoService, private router: Router) {
    
    this.miComment = new Comentario();
    this.user = new Usuario();
    this.user.nombre = '';
    this.user.apellido = '';
    this.user.email = '';
    this.user.pwd = '';
   }

  ngOnInit() {
    let activo= window.localStorage.getItem('username');
    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }

  addnewComment(artista : Artista){
    console.log("new comment");
    this.miComment.artista = artista;
    this.user.username = window.localStorage.getItem('username');
    this.miComment.usuario = this.user; 
    console.log(window.localStorage.getItem('username'));
    console.log(this.miComment.usuario.username);
    this.technoService.insertComment(this.miComment);
    this.miComment = new Comentario();
  }

}
