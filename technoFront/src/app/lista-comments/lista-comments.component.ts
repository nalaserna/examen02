import { Component, OnInit, Input } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { Comentario } from 'src/app/model/Comentario';
import { TechnoService } from '../../services/techno.service';
import { Usuario } from 'src/app/model/Usuario';
import { Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-comments',
  templateUrl: './lista-comments.component.html',
  styleUrls: ['./lista-comments.component.css']
})
export class ListaCommentsComponent implements OnInit {
  [x: string]: any;

  constructor(api: TechnoService, private route: ActivatedRoute) { 
  this.api = api;
    this.artistaid = this.route.snapshot.params['id'];
    this.api.getCommentsByArtist(this.artistaid).subscribe(resp => {
      this.comments = resp;
    });
   }
   api: TechnoService;
   @Input() user: Usuario;
   @Input() mySelectedArtista: Artista;
   @Input() artistaid: number;
   @Input() comments: Array<Comentario>;
  ngOnInit() {
  }

}
