import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCommentsComponent } from './lista-comments.component';

describe('ListaCommentsComponent', () => {
  let component: ListaCommentsComponent;
  let fixture: ComponentFixture<ListaCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
