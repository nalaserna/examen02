import { Component, OnInit } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { TechnoService } from 'src/services/techno.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-artista',
  templateUrl: './create-artista.component.html',
  styleUrls: ['./create-artista.component.css']
})
export class CreateArtistaComponent implements OnInit {
  miArtista : Artista;

  constructor(private technoService: TechnoService, private router: Router) {

    this.miArtista = new Artista();
   }
   
  ngOnInit() {
    let activo = window.localStorage.getItem('username')
    console.log(activo);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }

  addnewArtista(){
    console.log("new comment");
    this.technoService.newArtista(this.miArtista);
    this.miArtista = new Artista();
  }

}
