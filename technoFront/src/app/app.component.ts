import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'technoFront';

  constructor(private router: Router) {

  }

  public signOut() {
    window.localStorage.removeItem('username');
    this.router.navigateByUrl('/welcome');
  }
}


