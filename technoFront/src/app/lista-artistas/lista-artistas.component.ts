import { Component, OnInit, Input } from '@angular/core';
import { Artista } from 'src/app/model/Artista';
import { TechnoService } from 'src/services/techno.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-artistas',
  templateUrl: './lista-artistas.component.html',
  styleUrls: ['./lista-artistas.component.css']
})
export class ListaArtistasComponent implements OnInit {

  @Input() detalle: boolean;
  miArtista: Artista;
  artistas: Array<Artista>;

  constructor(private technoservice: TechnoService, private router: Router) {

    this.miArtista = new Artista();
    technoservice.getAllArtistas().subscribe(resp => {
      this.artistas = resp;
    });
  }

  ngOnInit() {
    let activo = window.localStorage.getItem('username')
    console.log(activo);
    
    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }

}
