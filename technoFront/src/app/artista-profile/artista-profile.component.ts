import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { TechnoService } from 'src/services/techno.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Artista } from 'src/app/model/Artista';


@Component({
  selector: 'app-artista-profile',
  templateUrl: './artista-profile.component.html',
  styleUrls: ['./artista-profile.component.css']
})
export class ArtistaProfileComponent implements OnInit {
  [x: string]: any;

  constructor(api: TechnoService, private route: ActivatedRoute, private router: Router) {
    this.api = api;
    this.artistaid = this.route.snapshot.params['id'];
    this.api.viewArtista(this.artistaid).subscribe(resp => {
      console.log('Artista ID: ' + this.artistaid);
      this.artista = resp;
    });
  }


  api: TechnoService;
  @Input() mySelectedArtista: Artista;
  @Input() artista: Artista;
  @Input() artistaid: number;
  @Input() artistas: Array<Artista>;
  mostrarBoton: false;

  ngOnInit() {
    let activo = window.localStorage.getItem('username')
    console.log(activo);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }



}
