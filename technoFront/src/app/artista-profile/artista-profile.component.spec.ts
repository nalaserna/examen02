import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistaProfileComponent } from './artista-profile.component';

describe('ArtistaProfileComponent', () => {
  let component: ArtistaProfileComponent;
  let fixture: ComponentFixture<ArtistaProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistaProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistaProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
